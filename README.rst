==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

=========
idem-helm
=========

The Idem Helm provider

About
=====

An Idem plug-in to manage Helm resources. Helm is a package manager for Kubernetes.

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring together concepts and wisdom from the history of computing in new ways to solve modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/saltstack/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/saltstack/pop/pop-create/>`__

Getting Started
===============

Prerequisites
-------------

* Python 3.7+
* git *(if installing from source or contributing to the project)*
* Idem

.. note::
  It is recommended that you install Idem using Poetry. Poetry is a tool for virtual environment and dependency management. See the `Idem Getting Started guide <https://docs.idemproject.io/getting-started/en/latest/topics/gettingstarted/installing.html>`_ for more information.

Installation
------------

You can install ``idem-helm`` from PyPI, a source repository, or a local directory.

Before you install ``idem-helm``, ensure that you are in the same directory as your ``pyproject.toml`` file. Optionally, you can specify the directory containing your ``pyproject.toml`` file by using the ``--directory=DIRECTORY (-C)`` option.

Install from PyPI
+++++++++++++++++

To install ``idem-helm`` from PyPI, run the following command:

.. code-block:: bash

  poetry add idem-helm

Install from source
+++++++++++++++++++

You can also install ``idem-helm`` directly from the source repository:

.. code-block:: bash

  poetry add git+https://gitlab.com/vmware/idem/idem-helm.git

If you don't specify a branch, Poetry uses the latest commit on the ``master`` branch.

Install from a local directory
++++++++++++++++++++++++++++++

Clone the ``idem-helm`` repository. Then run the following command to install from the cloned directory:

.. code-block:: bash

  poetry add ~/path/to/idem-helm


Usage
=====

Setup
-----

After installation, ``idem-helm`` execution and state modules are accessible to the pop *hub*.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop hub <https://pop-book.readthedocs.io/en/latest/main/hub.html#>`__

To use ``idem-helm`` execution and state modules to manage cluster resources, you need to set up authentication in one of the following ways.

**With environment variables**

Set KUBE_CONFIG_PATH and KUBE_CTX environment variables to the Kubernetes configuration file and context found in your ``kube_config`` file.

**In the Idem config file**

Edit the Idem config file to include the ``kube_config_path`` and ``context`` under account extras. Use the following example as a guideline.

.. code:: sls

    acct:
      extras:
        helm:
          default:
            kube_config_path: ~/.kube/config
            context: default

**In a credentials.yaml file**

Create or edit an Idem credentials.yaml file to add the ``kube_config_path`` and ``context`` to a Helm profile. Use the following example as a guideline.

..  code:: sls

    helm:
      default:
        kube_config_path: ~/.kube/config
        context: kubernetes-admin@kubernetes

For more about Idem credentials files, including recommended steps for encryption and environment variables, see `Authenticating with Idem <https://docs.idemproject.io/getting-started/en/latest/topics/gettingstarted/authenticating.html>`__

You are now ready to use idem-helm.

States
------

Idem SLS files use states to ensure that resources are in a desired configuration. An idem-helm SLS file supports three state functions: *present*, *absent*, and *describe*.

present
+++++++

The *present* function ensures that a resource exists. If a resource doesn't exist, running *present* creates it. If the resource already exists, running *present* might leave it unchanged, or update it if there are any configuration changes.

absent
++++++

The *absent* function ensures that a resource does not exist. If the resource exists, running *absent* deletes it. If the resource doesn't exist, running *absent* has no effect.

describe
++++++++

The *describe* function returns a list of all resources in the Kubernetes cluster of the same type as specified in the credential profile.

Accessing States
----------------

States can be accessed by their relative location in ``idem-helm/idem_helm/states``.

For example, a Helm release state can be created with the *present* function as shown in the following SLS file.

helm_release.sls:

.. code:: sls

    idem-helm-release-test:
      helm.release.present:
      - name: idem-redis
      - repository: https://charts.bitnami.com/bitnami
      - chart: redis
      - namespace: kube-system
      - resource_id: idem-redis
      - values:
            image:
                pullPolicy: IfNotPresent


The Idem command to create the preceding release state is:

.. code:: bash

    idem state $PWD/helm_release.sls

Current Supported Resources
---------------------------

helm
----

release
